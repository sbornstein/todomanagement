﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using ToDoManagement.Services.Services.ToDo;
using System.Threading.Tasks;
using Xunit;
using ToDoManagement.Common.Dto.ToDo;
using ToDoManagement.DataAccess.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using ToDoManagement.Services.AutoMapper;
using System.Linq;

namespace ToDoManagement.Test
{
    public class ToDoServicesTest
    {
        private readonly Mock<IUnitOfWork> _mockUow;
        private static IMapper _mapper;
        private readonly ToDoService _service;

        public ToDoServicesTest()
        {
            _mockUow = new Mock<IUnitOfWork>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ToDoMappingProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            _mapper = mapper;

            _service = new ToDoService(_mockUow?.Object, _mapper);
        }

        [Fact]
        public async Task All_GetsToDos_ReturnsToDos()
        {
            //arrange
            _mockUow.Setup(x => x.ToDos.All()).Returns(GetSampleToDo());

            //act
            var result = await _service.All();

            //assert
            Assert.NotNull(result);
            Assert.Equal(3, result.ToList().Count);
        }

        [Fact]
        public async Task GetById_GetsToDo_ReturnsToDo()
        {
            //arrange
            _mockUow.Setup(x => x.ToDos.GetById(1)).ReturnsAsync(GetSampleToDo().Result.First());

            //act
            var result = await _service.GetById(1);

            //assert
            Assert.NotNull(result);
            Assert.Equal(1, result.Id);
        }

        [Fact]
        public async Task Add_AddsToDo_ReturnsSuccess()
        {
            //arrange
            _mockUow.Setup(x => x.ToDos.Add(It.IsAny<DataAccess.Entities.ToDo>())).ReturnsAsync(true);

            //act
            var result = await _service.Add(_createToDoDto);

            //assert
            Assert.True(result);
        }

        [Fact]
        public async Task Update_UpdatesToDo_ReturnsSuccess()
        {
            //arrange
            _mockUow.Setup(x => x.ToDos.Update(It.IsAny<DataAccess.Entities.ToDo>())).ReturnsAsync(true);

            //act
            var result = await _service.Update(_updateToDoDto);

            //assert
            Assert.True(result);
        }

        [Fact]
        public async Task Delete_DeletesToDo_ReturnsSuccess()
        {
            //arrange
            _mockUow.Setup(x => x.ToDos.Delete(1)).ReturnsAsync(true);

            //act
            var result = await _service.Delete(1);

            //assert
            Assert.True(result);
        }

        private readonly CreateToDoDto _createToDoDto = new()
        {
            Name = "To Do 1",
            Description = "To Do 1 Description",
            DueDate = DateTime.Now.AddDays(2)
        };

        private readonly UpdateToDoDto _updateToDoDto = new()
        {
            Id = 2,
            Name = "To Do 2",
            Description = "To Do 2 Description Update",
            DueDate = DateTime.Now.AddDays(2)
        };

        private static Task<IEnumerable<DataAccess.Entities.ToDo>> GetSampleToDo()
        {
            IEnumerable<DataAccess.Entities.ToDo> output = new List<DataAccess.Entities.ToDo>
        {
            new DataAccess.Entities.ToDo
            {
                Id = 1,
                Name = "To Do 1",
                Description = "To Do 1 Description",
                DueDate = DateTime.Now.AddDays(2),
                DateCreated = DateTime.Now
            },
            new DataAccess.Entities.ToDo
            {
                Id = 2,
                Name = "To Do 2",
                Description = "To Do 2 Description",
                DueDate = DateTime.Now.AddDays(3),
                DateCreated = DateTime.Now
            },
            new DataAccess.Entities.ToDo
            {
                Id = 3,
                Name = "To Do 3",
                Description = "To Do 3 Description",
                DueDate = DateTime.Now.AddDays(4),
                DateCreated = DateTime.Now
            }
        };
            return Task.FromResult(output);
        }
    }
}
