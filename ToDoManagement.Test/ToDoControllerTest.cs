﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoManagement.Services.Services.ToDo;
using System.Threading.Tasks;
using ToDoManagement.UI.Controllers;
using Xunit;
using ToDoManagement.Common.Dto.ToDo;
using ToDoManagement.UI.Models;

namespace ToDoManagement.Test
{
    public class ToDoControllerTest
    {
        private readonly Mock<IToDoService> _mockService;
        private readonly ToDoController _controller;

        public ToDoControllerTest()
        {
            _mockService = new Mock<IToDoService>();
            _controller = new ToDoController(_mockService?.Object);
        }

        [Fact]
        //naming convention MethodName_expectedBehavior_StateUnderTest
        public async Task Index_ActionExecutes_ReturnsViewForIndex()
        {
            //arrange
            _mockService.Setup(x => x.All())
            .Returns(GetSampleToDo());

            //act
            var result = await _controller.Index();

            //assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task GetToDo_ListOfToDo_ToDoExistsInRepo()
        {
            //arrange
            var toDo = await GetSampleToDo();
            _mockService.Setup(x => x.All())
                .Returns(GetSampleToDo());

            //act
            var result = await _controller.Index();

            //assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var toDos = Assert.IsType<ToDoModel>(viewResult.Model);
            Assert.Equal(toDo.ToList().Count, toDos.ToDoList.ToList().Count);
        }

        [Fact]
        public async Task GetToDo_ReturnToDoById_ReturnToDo()
        {
            //arrange
            var toDoActual = await GetSampleToDo();
            var toDo = toDoActual.ToList().FirstOrDefault(x => x.Id == 1);
            _mockService.Setup(x => x.GetById(1).Result)
            .Returns(toDo);

            //act
            var result = await _controller.GetToDo(1);
            var jsonResult = Assert.IsType<JsonResult>(result);

            //assert
            Assert.Equal(toDo, jsonResult.Value);
        }

        [Fact]
        public async Task GetToDo_DoesNotReturnToDoById_DoesNotReturnToDo()
        {
            //arrange
            var toDoActual = await GetSampleToDo();
            var toDo = toDoActual.ToList().FirstOrDefault(x => x.Id == 2);
            _mockService.Setup(x => x.GetById(1).Result)
            .Returns(toDoActual.ToList().FirstOrDefault(x => x.Id == 1));

            //act
            var result = await _controller.GetToDo(1);
            var jsonResult = Assert.IsType<JsonResult>(result);

            //assert
            Assert.NotEqual(toDo, jsonResult.Value);
        }

        [Fact]
        public async Task Create_CreateNewToDo_RedirectToIndex()
        {
            //arrange

            //act
            var toDoModel = new ToDoModel();
            var toDo = new CreateToDoDto { Name = "Some To Do", Description = "Some Description", DueDate = DateTime.Now };
            toDoModel.CreateToDo = toDo;
            var result = await _controller.CreateToDo(toDoModel);
            var viewResult = Assert.IsType<RedirectToActionResult>(result);

            //assert
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task Create_FailedToCreateNewToDo_RedirectToIndex()
        {
            //arrange
            _controller.ModelState.AddModelError("Name", "Name is required");

            //act
            var toDoModel = new ToDoModel();
            var toDo = new CreateToDoDto { Description = "Some Description", DueDate = DateTime.Now };
            toDoModel.CreateToDo = toDo;
            var result = await _controller.CreateToDo(toDoModel);
            var viewResult = Assert.IsType<RedirectToActionResult>(result);

            //assert
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task Update_UpdateToDo_RedirectToIndex()
        {
            //arrange
            //act
            var toDoModel = new ToDoModel();
            var toDo = new UpdateToDoDto { Id = 1, Name = "Some To Do", Description = "Some Description", DueDate = DateTime.Now };
            toDoModel.UpdateToDo = toDo;
            var result = await _controller.UpdateToDo(toDoModel);
            var viewResult = Assert.IsType<RedirectToActionResult>(result);

            //assert
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task Update_FailedToUpdateToDo_RedirectToIndex()
        {
            //arrange
            _controller.ModelState.AddModelError("Name", "Name is required");

            //act
            var toDoModel = new ToDoModel();
            var toDo = new UpdateToDoDto { Id = 1, Description = "Some Description", DueDate = DateTime.Now };
            toDoModel.UpdateToDo = toDo;
            var result = await _controller.UpdateToDo(toDoModel);
            var viewResult = Assert.IsType<RedirectToActionResult>(result);

            //assert
            Assert.Equal("Index", viewResult.ActionName);
        }


        [Fact]
        public async Task Delete_DeleteToDo_RedirectToIndex()
        {
            //arrange
            //act
            var result = await _controller.DeleteToDo(1);
            var viewResult = Assert.IsType<RedirectToActionResult>(result);

            //assert
            Assert.Equal("Index", viewResult.ActionName);
        }

        private static Task<IEnumerable<ToDoDto>> GetSampleToDo()
        {
            IEnumerable<ToDoDto> output = new List<ToDoDto>
        {
            new ToDoDto
            {
                Id = 1,
                Name = "To Do 1",
                Description = "To Do 1 Description",
                DueDate = DateTime.Now.AddDays(2),
                DateCreated = DateTime.Now
            },
            new ToDoDto
            {
                Id = 2,
                Name = "To Do 2",
                Description = "To Do 2 Description",
                DueDate = DateTime.Now.AddDays(3),
                DateCreated = DateTime.Now
            },
            new ToDoDto
            {
                Id = 3,
                Name = "To Do 3",
                Description = "To Do 3 Description",
                DueDate = DateTime.Now.AddDays(4),
                DateCreated = DateTime.Now
            }
        };
            return Task.FromResult(output);
        }
    }
}
