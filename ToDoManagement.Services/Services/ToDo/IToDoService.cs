﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoManagement.Common.Dto.ToDo;

namespace ToDoManagement.Services.Services.ToDo
{
    public interface IToDoService
    {
        Task<IEnumerable<ToDoDto>> All();
        Task<ToDoDto> GetById(int id);
        Task<bool> Add(CreateToDoDto entity);
        Task<bool> Delete(int id);
        Task<bool> Update(UpdateToDoDto entity);
    }
}
