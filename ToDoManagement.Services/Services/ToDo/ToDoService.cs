﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoManagement.Common.Dto.ToDo;
using ToDoManagement.DataAccess.Interfaces;

namespace ToDoManagement.Services.Services.ToDo
{
    public class ToDoService : IToDoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ToDoService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ToDoDto>> All()
        {
            var taskList = await _unitOfWork.ToDos.All();
            var result = _mapper.Map<IEnumerable<DataAccess.Entities.ToDo>, IEnumerable<ToDoDto>>(taskList);
            return result;
        }

        public async Task<ToDoDto> GetById(int id)
        {
            var toDo = await _unitOfWork.ToDos.GetById(id);
            var result = _mapper.Map<DataAccess.Entities.ToDo, ToDoDto>(toDo);
            return result;
        }

        public async Task<bool> Add(CreateToDoDto entity)
        {
            var toDo = _mapper.Map<CreateToDoDto, DataAccess.Entities.ToDo>(entity);
            toDo.DateCreated = DateTime.Now;
            var result = await _unitOfWork.ToDos.Add(toDo);
            if (result) await _unitOfWork.CompleteAsync();
            return result;
        }

        public async Task<bool> Update(UpdateToDoDto entity)
        {
            var toDo = _mapper.Map<UpdateToDoDto, DataAccess.Entities.ToDo>(entity);
            var result = await _unitOfWork.ToDos.Update(toDo);
            if (result) await _unitOfWork.CompleteAsync();
            return result;
        }

        public async Task<bool> Delete(int id)
        {
            var result = await _unitOfWork.ToDos.Delete(id);
            if (result) await _unitOfWork.CompleteAsync();
            return result;
        }
    }
}
