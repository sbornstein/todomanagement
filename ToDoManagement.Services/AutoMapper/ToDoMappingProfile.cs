﻿using AutoMapper;
using ToDoManagement.Common.Dto.ToDo;
using ToDoManagement.DataAccess.Entities;

namespace ToDoManagement.Services.AutoMapper
{
    public class ToDoMappingProfile : Profile
    {
        public ToDoMappingProfile()
        {
            CreateMap<CreateToDoDto, ToDo>();
            CreateMap<UpdateToDoDto, ToDo>();
            CreateMap<ToDo, ToDoDto>();
        }
    }
}
