﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoManagement.DataAccess.Data;
using ToDoManagement.DataAccess.Interfaces;

namespace ToDoManagement.DataAccess.Repositories
{
    public class ToDoRepository : GenericRepository<Entities.ToDo>, IToDoRepository
    {
        public ToDoRepository(ApplicationDbContext context) : base(context) { }
        public override async Task<IEnumerable<Entities.ToDo>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch (Exception)
            {
                return new List<Entities.ToDo>();
            }
        }

        public override async Task<bool> Update(Entities.ToDo entity)
        {
            try
            {
                var existingTask = await dbSet.Where(x => x.Id == entity.Id)
                                                    .FirstOrDefaultAsync();

                if (existingTask == null)
                    return await Add(entity);

                existingTask.Name = entity.Name;
                existingTask.Description = entity.Description;
                existingTask.DueDate = entity.DueDate;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id)
                                        .FirstOrDefaultAsync();

                if (exist == null) return false;

                dbSet.Remove(exist);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
