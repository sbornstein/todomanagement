﻿using System;
using System.Threading.Tasks;
using ToDoManagement.DataAccess.Interfaces;
using ToDoManagement.DataAccess.Repositories;

namespace ToDoManagement.DataAccess.Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext _context;

        public IToDoRepository ToDos { get; private set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            ToDos = new ToDoRepository(context);
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
