﻿using System.Threading.Tasks;

namespace ToDoManagement.DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        IToDoRepository ToDos { get; }
        Task CompleteAsync();
    }
}
