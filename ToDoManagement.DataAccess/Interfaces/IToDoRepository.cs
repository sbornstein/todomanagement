﻿using ToDoManagement.DataAccess.Entities;

namespace ToDoManagement.DataAccess.Interfaces
{
    public interface IToDoRepository : IGenericRepository<ToDo>
    {
    }
}
