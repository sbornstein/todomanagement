﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using ToDoManagement.Services.Services.ToDo;
using ToDoManagement.UI.Models;

namespace ToDoManagement.UI.Controllers
{
    public class ToDoController : Controller
    {
        private readonly IToDoService _toDoService;

        public ToDoController(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }

        public async Task<IActionResult> Index()
        {
            var model = new ToDoModel();
            var toDo = await _toDoService.All();
            model.ToDoList = toDo;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateToDo(ToDoModel model)
        {
            throw new ArgumentException("Index is out of range");
            await _toDoService.Add(model.CreateToDo);
            return RedirectToAction("Index", "ToDo");
        }

        [HttpGet]
        public async Task<IActionResult> GetToDo(int toDoId)
        {
            var task = await _toDoService.GetById(toDoId);
            return Json(task);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateToDo(ToDoModel model)
        {
            await _toDoService.Update(model.UpdateToDo);
            return RedirectToAction("Index", "ToDo");
        }

        public async Task<IActionResult> DeleteToDo(int id)
        {
            await _toDoService.Delete(id);
            return RedirectToAction("Index", "ToDo");
        }
    }
}
