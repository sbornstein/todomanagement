﻿using System.Collections.Generic;
using ToDoManagement.Common.Dto.ToDo;

namespace ToDoManagement.UI.Models
{
    public class ToDoModel
    {
        public IEnumerable<ToDoDto> ToDoList { get; set; }
        public CreateToDoDto CreateToDo { get; set; }
        public UpdateToDoDto UpdateToDo { get; set; }
    }
}
