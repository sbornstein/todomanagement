﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoManagement.Common.Dto.ToDo
{
    public class CreateToDoDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
    }
}
