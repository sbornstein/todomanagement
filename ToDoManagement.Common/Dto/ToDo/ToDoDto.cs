﻿using System;

namespace ToDoManagement.Common.Dto.ToDo
{
    public class ToDoDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
