# To Do Management

To Do Management PDHI Code Test

## Getting started

Visual Studio 2019 based project for PDHI coding test.  Architecture was built with seperation of concerns with the following layers:

Projects
- ToDoManagement.UI (.NET Core 5 MVC Web Application UI)
    - Bootstrap was used for styling and js libraries for this demo.
    - Controllers, Models and Views
- ToDoManagement.Common (.NET Core 5 Class Library)
    - DTOs
- ToDoManagement.Services (.NET Core 5 Class Library)
    - AutoMapper
    - Services
- ToDoManagement.DataAccess (.NET Core 5 Class library)
    - EF Core - In Memory Database
    - Repository Unit of Work
- ToDoManagement.Test (.NET Core 5 Test Project)
    - Controller Tests
    - Service Tests
    - DotCover was used for checking code coverage VS 2019 Enterprise as trial was up local using Community edition.

## Project status
Limited time does not have detailed error handling 'to do'.  Enterprise development many times use Worker classes to handle main business logic and pass through to Service layer that would either direct to caches or database calls if not cached.  Also to do picker should have time as well currently only using date picker but could be added.
